var express = require('express');
var request = require('request');
var bodyParser = require('body-parser');
var opener = require('opener');

var app = express();

var now = function(){
    return new Date().toLocaleTimeString();
};

var printColor = function(text, color){
    console.log(color + text + '\x1b[37m');
};

var printRed = function(text){
    printColor(text, '\x1b[31m');
};

var printGreen = function(text){
    printColor(text, '\x1b[32m');
};

app.use(bodyParser.json());
app.use(express.static('public'));
app.use('/lib', express.static('node_modules'));

app.post('/route', function(req, resp){
    var requestDetails = req.body;
    if(requestDetails.port){
        requestDetails.port = ":" + requestDetails.port;
    }
    var requestUri = 'http://' +
        requestDetails.credentials +
        requestDetails.server +
        requestDetails.port +
        requestDetails.path;

    request({
        method: requestDetails.method,
        uri: requestUri,
        body: requestDetails.body
    }, function(error, response, body){
        if(error){
            printRed(now() + ' - ' + error);
            resp.status(500).send(error.code);
        }else{
            if(response.statusCode < 300){
                printGreen(now() +
                           ' - ' +
                           requestDetails.method +
                           ' ' +
                           requestUri +
                           ': ' + 
                           response.statusCode);
                resp.json(JSON.parse(body));
            }else{
                printRed(now() + 
                              ' - ' + 
                              requestDetails.method +
                              ' ' +
                              requestUri + 
                              ': ' + 
                              response.statusCode);
                resp.status(response.statusCode).send('');
            }
        }
    });
});

exports.listen = function(port){
    app.listen(port);

    console.log('listening on port ' + port);
    opener('http://localhost:' + port + '/');
};
